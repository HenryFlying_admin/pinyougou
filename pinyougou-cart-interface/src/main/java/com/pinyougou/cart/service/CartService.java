package com.pinyougou.cart.service;

import com.pinyougou.pojogroup.Cart;

import java.util.List;

/**
 * 购物车服务接口
 */
public interface CartService {
	/**
	 * 添加商品到购物车
	 * @param cartList
	 * @param itemId
	 * @param num
	 * @return
	 */
	List<Cart>  addGoods2CartList(List<Cart> cartList,Long itemId,Integer num);

	/**
	 * 从redis中提取购物车
	 * @param username
	 * @return
	 */
	List<Cart> findCartListFromRedis(String username);

	/**
	 * 将购物车列表存入redis
	 * @param username
	 */
	void saveCartList2Redis(String username, List<Cart> list);

	/**
	 * 合并购物车
	 * @param list1
	 * @param list2
	 * @return
	 */
	List<Cart> mergeCartList(List<Cart> list1, List<Cart> list2);
}
