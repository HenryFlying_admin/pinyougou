package com.pinyougou.page.service.impl;

import com.pinyougou.page.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.util.Arrays;

@Component
public class PageDeleteListener implements MessageListener {
    @Autowired
    private ItemPageService itemPageService;


    @Override
    public void onMessage(Message message) {
        ObjectMessage objectMessage = (ObjectMessage) message;

        try {
            Long[] goodsIds = (Long[]) objectMessage.getObject();

            System.out.println("监听到消息:"+ Arrays.toString(goodsIds));
            boolean b = itemPageService.deleteItemHtml(goodsIds);
            System.out.println("删除商品详情页结果:"+b);

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}