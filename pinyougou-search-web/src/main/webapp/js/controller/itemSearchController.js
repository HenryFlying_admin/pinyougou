app.controller('itemSearchController',function ($scope,$location,searchService) {
    //定义搜索对象的结构
    $scope.searchMap={'keywords':'','category':'','brand':'','spec':{},'price':'','pageNum':1,'pageSize':40,'sort':'','sortField':''}


    //搜索
    $scope.search=function () {
        $scope.searchMap.pageNum = parseInt($scope.searchMap.pageNum);
        searchService.search($scope.searchMap).success(
            function (response) {
                $scope.resultMap = response;
                buildPageLabel();
                $scope.searchMap.pageNum=1;
            }
        )
    }
    buildPageLabel=function(){
        //构建分页栏
        $scope.pageLabel = [];
        var firstPage=1;
        var lastPage=$scope.resultMap.totalPages;
        if (lastPage > 5) {
            if($scope.searchMap.pageNum<=3){
                lastPage=5;
            }else if($scope.searchMap.pageNum>=lastPage-2){
                firstPage=lastPage-4;
            }else {
                firstPage=$scope.searchMap.pageNum-2;
                lastPage=$scope.searchMap.pageNum+2;
            }
        }
        for(var i=firstPage;i<=lastPage;i++){
            $scope.pageLabel.push(i);
        }
        $scope.lastDot = lastPage<$scope.resultMap.totalPages;
    }
    //添加搜索项  改变searchMap的值
    $scope.addSearchItem=function (key,value) {
        if (key == 'category' || key == 'brand'||key=='price') {
            //如果用户点击的是分类或品牌   searchMap重新赋值就可以了
            $scope.searchMap[key]=value;
        }else{
            //用户点击的是规格  多选的没实现
            $scope.searchMap.spec[key]=value;
        }
        $scope.search();
    }

    //撤销搜索项
    $scope.removeSearchItem=function (key) {
        if (key == 'category' || key == 'brand'||key=='price') {
            //如果用户点击的是分类或品牌   searchMap重新赋值就可以了
            $scope.searchMap[key]="";
        }else{
            //用户点击的是规格  多选的没实现
            delete $scope.searchMap.spec[key];
        }
        $scope.search();
    }
    //分页查询
    $scope.queryByPage=function (pageNum) {
        if(pageNum<1||pageNum>$scope.resultMap.totalPages){
            return;
        }
        $scope.searchMap.pageNum=pageNum;
        $scope.search();
    }
    //判断当前页是否为第一页
    $scope.isTopPage=function () {
        if ($scope.searchMap.pageNum == 1) {
            return true;
        }
        return false;
    }

    //判断当前页是否为最后一页
    $scope.isEndPage=function () {
        if ($scope.searchMap.pageNum ==$scope.resultMap.totalPages ) {
            return true;
        }
        return false;
    }

    //排序查询
    $scope.sortSearch=function (sortField, sort) {
        $scope.searchMap.sortField=sortField;
        $scope.searchMap.sort=sort;
        $scope.search();
    }
    //判断关键字是否是品牌
    $scope.keywordsIsBrand=function () {
        for (var i = 0; i < $scope.resultMap.brandList.length; i++) {
            if ($scope.searchMap.keywords.indexOf($scope.resultMap.brandList[i].text)>=0) {
                return true;
            }
        }
        return false;
    }

    //加载关键字
    $scope.loadkeywords=function () {
        $scope.searchMap.keywords = $location.search()['keywords'];
        $scope.search();
    }
})