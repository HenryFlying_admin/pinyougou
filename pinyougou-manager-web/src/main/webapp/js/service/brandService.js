app.service("brandService",function ($http) {
            this.findAll=function () {
				return $http.get('/brand/findAll.do');
            };

            this.findPage=function(page,size) {
                return $http.post('/brand/findPage.do?pageNum=' + page + '&pageSize=' + size);
            };

            this.findOne=function (id) {
                return $http.get('/brand/findOne.do?id=' + id);
            };

            this.del=function (selectIds) {
                return $http.get('/brand/delete.do?ids='+selectIds);
            };

            this.add=function (entity) {
				return $http.post('/brand/add.do',entity);
            };

            this.update=function (entity) {
                return $http.post('/brand/update.do',entity);
            };

            this.search=function(page,size,searchEntity) {
                return $http.post('/brand/search.do?pageNum=' + page + '&pageSize=' + size, searchEntity);
            };
            
            this.selectOptionList=function () {
                return $http.get('/brand/selectOptionList.do');
            }
        })