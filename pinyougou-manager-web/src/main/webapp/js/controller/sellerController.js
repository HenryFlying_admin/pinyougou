 //控制层 
app.controller('sellerController' ,function($scope,$controller   ,sellerService){	
	
	$controller('baseController',{$scope:$scope});//继承
	
    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
		sellerService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){			
		sellerService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(id){				
		sellerService.findOne(id).success(
			function(response){
				$scope.entity= response;					
			}
		);				
	}
	
	//保存 
	$scope.save=function(){				
		var serviceObject;//服务层对象  				
		if($scope.entity.id!=null){//如果有ID
			serviceObject=sellerService.update( $scope.entity ); //修改  
		}else{
			serviceObject=sellerService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
		        	$scope.reloadList();//重新加载
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		sellerService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
					$scope.selectIds=[];
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		sellerService.search(page,rows,$scope.selectStatusList,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//更改状态
	$scope.updateStatus=function(sellerId,status){
		sellerService.updateStatus(sellerId,status).success(
			function(response){
				if(response.success){
					$scope.reloadList();//重新加载
				}else{
					alert(response.message);
				}				
			}
		);		
	}

    $scope.castStatus=function (status) {
		var value="";
		if(status=='0'){
			value="待审核"
		} else if(status=='1'){
            value="审核通过"
        }else if(status=='2'){
            value="审核未通过"
        }else if(status=='3'){
            value="关闭"
        }
        return value;
    }

    $scope.selectStatusList = [];//用户勾选的id集合

    $scope.updateSelection=function ($event,status) {
        if ($event.target.checked) {
            $scope.selectStatusList.push(status);//push方法向集合添加元素
        }else {
            var index1=$scope.selectStatusList.indexOf(status);//获取id这个值在集合或者数组中的位置
            $scope.selectStatusList.splice(index1,1);//参数1:移除的位置  参数2:移除的个数
        }
    }

    $scope.updateSelection1=function ($event,status) {
		var index = $scope.selectStatusList.indexOf(null);

        if ($event.target.checked) {
        	if(status==null){
                $scope.selectStatusList = [];
			}else if(index>=0){
                $scope.selectStatusList.splice(index,1);
			}
            $scope.selectStatusList.push(status);//push方法向集合添加元素
        }else {
            var index1=$scope.selectStatusList.indexOf(status);//获取id这个值在集合或者数组中的位置
            $scope.selectStatusList.splice(index1,1);//参数1:移除的位置  参数2:移除的个数
        }
    }
    
});	
