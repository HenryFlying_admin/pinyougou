package com.pinyougou.manager.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {

    @RequestMapping("/select/{cid}")
    public String showPage(HttpServletRequest request, @PathVariable int cid){
        ServletContext sc = request.getSession().getServletContext();
        WebApplicationContext fu= ContextLoader.getCurrentWebApplicationContext();//父容器
//        Object bean = fu.getBean("country1");
        //Object bean2 = fu.getBean("pageController");//父容器不能访问子容器controller放在spingmvc容器中（子容器）
        //子容器,MyServletName不同，第二个参数不加则获取父容器
        WebApplicationContext zi = WebApplicationContextUtils.getWebApplicationContext(sc,"org.springframework.web.servlet.FrameworkServlet.CONTEXT.springmvc");
        Object bean2 = zi.getBean("brandController");
//        Object bean3 = fu.getBean("countryService");
        System.out.println(zi.hashCode());
        System.out.println(fu.hashCode());
//        System.out.println((Country)bean);
        System.out.println(bean2);
//        System.out.println(bean3);
        return "index";
    }
}
