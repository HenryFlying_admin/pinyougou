 //控制层 
app.controller('itemController' ,function($scope){	
	
	$scope.specificationItems={};//存储用户选择的规格
	//$scope.num=1;
	$scope.addNum=function(x){
		$scope.num+=x;
		if($scope.num<1){
			$scope.num=1;
		}
	}
	//用户选择规格
	$scope.selectSpecification=function(key,value){
		$scope.specificationItems[key]=value;
		searchSku();
	}
	//判断某规格是否被选中
	$scope.isSelected = function(key,value){
		if($scope.specificationItems[key]==value){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.sku={};//当前选择的SKU
	//加载默认的SKU
	$scope.loadSku=function(){
		$scope.sku=skuList[0];
		$scope.specificationItems=JSON.parse(JSON.stringify($scope.sku.spec));
	}
	//匹配两个对象是否相等
	matchObject=function(map1,map2){
		if(Object.keys(map1).length!=Object.keys(map2).length){
			return false;
		}
		for(var key in map1){
			if(map1[key]!=map2[key]){
				return false;
			}
		}
		return true;
	}
	//根据规格查找SKU
	searchSku=function(){
		for(var i=0;i<skuList.length;i++){
			if(matchObject(skuList[i].spec,$scope.specificationItems)){
				$scope.sku=skuList[i];
				return;
			}
		}
		$scope.sku={id:0,title:'',title:'-----'}
	}
	//添加商品到购物车
	$scope.add2Cart=function(){
		alert('SKUID='+$scope.sku.id);
	}
    
});	
