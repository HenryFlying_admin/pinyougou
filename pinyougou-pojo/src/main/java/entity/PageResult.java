package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页结果类
 *
 * 由于mybatis的PageHelper包返回的Page对象在web工程并没有引入,日志会报警告,所以这里对返回web工程的类型进行一下转换
 * if(rows!=null)
 *             this.rows=new ArrayList<>(rows);
 */
@SuppressWarnings("unchecked")
public class PageResult implements Serializable {
    private long total;//总记录数
    private List rows;//当前页记录

    public PageResult(long total, List rows) {
//        Collections.copy(this.rows,rows);
        this.total = total;
        if(rows!=null)
            this.rows=new ArrayList<>(rows);
//        this.rows = rows;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
