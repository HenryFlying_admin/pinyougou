//购物车服务层
app.service('cartService',function ($http) {
    //购物车列表
    this.findCartList=function () {
        return $http.get('cart/findCartList.do');
    }

    //添加商品到购物车
    this.addGoods2CartList=function (itemId, num) {
        return $http.get('/cart/addGoods2CartList.do?itemId='+itemId+'&num='+num);
    }

    this.sum=function (cartList) {
        var totalValue={totalNum:0, totalFee: 0};

        for (var i = 0; i < cartList.length; i++) {
            var cart = cartList[i];
            for (var j = 0; j < cart.orderItemList.length;j++) {
                var orderItem = cart.orderItemList[j];//购物车明细
                totalValue.totalNum+=orderItem.num;
                totalValue.totalFee+=orderItem.totalFee;
            }
        }
        return totalValue;
    }
})