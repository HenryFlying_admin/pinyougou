app.controller('cartController',function ($scope,cartService) {

    $scope.findCartList=function () {
        cartService.findCartList().success(
            function (response) {
                $scope.cartList=response;
                $scope.totalValue=cartService.sum($scope.cartList);
            }
        )
    }

    $scope.addGoods2CartList=function (itemId,num) {
        cartService.addGoods2CartList(itemId,num).success(
            function (response) {
                if(response.success){
                    //如果成功,刷新列表
                    $scope.findCartList();
                }else {
                    alert(response.message);
                }
            }
        )
    }

    // sum=function () {
    //     $scope.totalNum=0;
    //     $scope.totalFee=0;//总金额
    //
    //     for (var i = 0; i < $scope.cartList.length; i++) {
    //         var cart = $scope.cartList[i];
    //         for (var j = 0; j < cart.orderItemList.length;j++) {
    //             var orderItem = cart.orderItemList[j];//购物车明细
    //             $scope.totalNum+=orderItem.num;
    //             $scope.totalFee+=orderItem.totalFee;
    //         }
    //     }
    // }

})