package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.pojogroup.Cart;
import entity.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import util.CookieUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	@Reference
	private CartService cartService;

	@RequestMapping("/findCartList")
	public List<Cart> findCartList(){
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println("当前登录人:"+name);

		String cartListStr = CookieUtil.getCookieValue(request, "cartList", "UTF-8");
		if(StringUtils.isEmpty(cartListStr))
			cartListStr = "[]";
		List<Cart> cartList_cookie = JSON.parseArray(cartListStr, Cart.class);


		if ("anonymousUser".equals(name)) {//如果未登录
			//从cookie中提取购物车
			return cartList_cookie;
		} else {
			//如果已登录,先获取redis购物车,然后再与本地cookie购物车合并
			List<Cart> cartList_redis = cartService.findCartListFromRedis(name);
			if (cartList_cookie.size() == 0) {
				//如果本地购物车不存在数据,直接返回redis中的购物车即可
				return cartList_redis;
			}
			List<Cart> cartList = cartService.mergeCartList(cartList_cookie, cartList_redis);
			cartService.saveCartList2Redis(name,cartList);
			//清空本地cookie
			CookieUtil.deleteCookie(request,response,"cartList");
			return  cartList;
		}


	}
	@RequestMapping("/addGoods2CartList")
	public Result addGoods2CartList(Long itemId,Integer num){


		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println("当前登录人:"+name);

		//1.提取购物车
		//2.调用服务方法操作购物车
		//3.保存新的购物车
		try {
			List<Cart> cartList = findCartList();
			cartList = cartService.addGoods2CartList(cartList, itemId, num);

			if ("anonymousUser".equals(name)) {
				String cartListStr = JSON.toJSONString(cartList);
				CookieUtil.setCookie(request, response, "cartList", cartListStr, 3600 * 24, "UTF-8");
			}else {
				cartService.saveCartList2Redis(name,cartList);
			}

			return new Result(true, "存入购物车成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "存入购物车失败");
		}
	}
}
