package com.pinyougou.shop.controller;

import entity.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import util.FastdfsClient;

@RestController
public class UploadController {
    @Value("${FILE_SERVER_URL}")
    private String FILE_SERVER_URL;

    @RequestMapping("/upload")
    public Result upload(@RequestBody  MultipartFile file){
        String originalFilename = file.getOriginalFilename();//获取文件名
        String extName = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);//获取文件扩展名
        try {
            FastdfsClient fastdfsClient = new FastdfsClient("classpath:config/fastdfs.conf");
            String fileId = fastdfsClient.uploadFile(file.getBytes(), extName);

            String url = FILE_SERVER_URL+fileId;
            return new Result(true, url);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "上传失败");
        }
    }
}
