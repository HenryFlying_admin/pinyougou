package com.pinyougou.cart.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbOrderItem;
import com.pinyougou.pojogroup.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
@Service
public class CartServiceImpl implements CartService {
	@Autowired
	private TbItemMapper itemMapper;
	@Autowired
	private RedisTemplate redisTemplate;
//	private

	@Override
	public List<Cart> addGoods2CartList(List<Cart> cartList, Long itemId, Integer num) {

		//1.根据SKUID查询商品明细SKU对象
		//2.根据SKU对象得到商家ID
		//3.根据商家ID在购物车列表中查询购物车对象
		//4.如果购物车列表中不存在该商家的购物车
		//4.1创建一个新的购物车对象
		//4.2将新的购物车对象添加到购物车列表中

		//5.如果购物车列表中存在该商家的购物车
		//5.1判断该商品是否在该购物车的明细列表中存在
		//5.2如果不存在,创建新的购物车明细对象,并添加到该购物车的明细列表中
		//5.3如果存在,在原有的数量上添加数量,并且更新金额

		TbItem item = itemMapper.selectByPrimaryKey(itemId);
		if(item==null){
			throw new RuntimeException("商品不存在");
		}
		if (!"1".equals(item.getStatus())) {
			throw new RuntimeException("商品状态不合法");
		}

		String sellerId = item.getSellerId();
		Cart cart = searchCartBySellerId(cartList, sellerId);
		if (cart == null) {
			cart=new Cart();
			cart.setSellerId(sellerId);
			cart.setSellerName(item.getSeller());//商家名称
			List<TbOrderItem> orderItemList = new ArrayList<>();
			TbOrderItem orderItem = createOrderItem(item, num);
			orderItemList.add(orderItem);

			cart.setOrderItemList(orderItemList);
			cartList.add(cart);
		} else {
			TbOrderItem orderItem = searchOrderItemByItemId(cart.getOrderItemList(), itemId);
			if (orderItem == null) {
				orderItem = createOrderItem(item, num);
				cart.getOrderItemList().add(orderItem);
			}else {
				orderItem.setNum(orderItem.getNum()+num);
				orderItem.setTotalFee(item.getPrice().multiply(BigDecimal.valueOf(orderItem.getNum())));
				//当明细的数量小于等于0,移除此明细
				if(orderItem.getNum()<=0){
					cart.getOrderItemList().remove(orderItem);
				}
				//当购物车的明细列表数量为0,在购物车列表中移除此购物车对象
				if (cart.getOrderItemList().size() == 0) {
					cartList.remove(cart);
				}
			}

		}


		return cartList;
	}

	@Override
	public List<Cart> findCartListFromRedis(String username) {
		System.out.println("从redis中提取购物车"+username);
		List<Cart> cartList = (List<Cart>) redisTemplate.boundHashOps("cartList").get(username);
		if(cartList==null)
			cartList = new ArrayList<>();
		return cartList;
	}

	@Override
	public void saveCartList2Redis(String username, List<Cart> list) {
		System.out.println("向redis中存入购物车"+username);
		redisTemplate.boundHashOps("cartList").put(username,list);
	}

	@Override
	public List<Cart> mergeCartList(List<Cart> list1, List<Cart> list2) {
		//正常来说2个购物车列表  一个应该是本地,另一个应该是远端的
		for (Cart cart : list2) {
			for (TbOrderItem orderItem : cart.getOrderItemList()) {
				addGoods2CartList(list1, orderItem.getItemId(), orderItem.getNum());
			}
		}
		return list1;
	}

	/**
	 * 创建购物车明细对象
	 * @param item
	 * @param num
	 * @return
	 */
	private TbOrderItem createOrderItem(TbItem item, Integer num) {
		TbOrderItem orderItem = new TbOrderItem();//创建购物车明细对象
		orderItem.setGoodsId(item.getGoodsId());
		orderItem.setItemId(item.getId());
		orderItem.setNum(num);
		orderItem.setPicPath(item.getImage());
		orderItem.setPrice(item.getPrice());
		orderItem.setSellerId(item.getSellerId());
		orderItem.setTotalFee(item.getPrice().multiply(BigDecimal.valueOf(num)));
		return orderItem;
	}

	private Cart searchCartBySellerId(List<Cart> cartList,String sellerId){
		for (Cart cart : cartList) {
			if(cart.getSellerId().equals(sellerId))
				return cart;
		}
		return null;
	}

	/**
	 * 根据SKUID在购物车明细列表中查询购物车明细对象
	 * @param orderItemList
	 * @param itemId
	 * @return
	 */
	private TbOrderItem searchOrderItemByItemId(List<TbOrderItem> orderItemList,Long itemId) {
		for (TbOrderItem orderItem : orderItemList) {
			if (orderItem.getItemId().longValue() == itemId.longValue()) {
				return orderItem;
			}
		}
		return null;
	}
}
