package com.pinyougou.sellergoods.service;

import com.pinyougou.pojo.TbBrand;
import entity.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 品牌接口
 */
public interface BrandService {
    List<TbBrand> findAll();

    /**
     * 品牌分页
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageResult findPage(int pageNum, int pageSize);
    //新增品牌
    void add(TbBrand brand);
    //根据id查询实体
    TbBrand findOne(Long id);
    //把修改后的内容存回去
    void update(TbBrand brand);
    //批量删除
    void delete(Long[] ids);
    //条件查询
    PageResult findPage(TbBrand brand,int pageNum, int pageSize);
    //返回下拉列表数据
    List<Map> selectOptionList();
}
